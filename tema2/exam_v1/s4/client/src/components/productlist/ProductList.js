import React from 'react';

export class ProductList extends React.Component{
    // eslint-disable-next-line
    constructor(props)
    {
        super(props);
        
    }
    
    render(){
        let products= this.props.source.map((product,index)=>{
            return <div key={index}>{product.productName}</div>;
        });
        return(
            <div>
            <h1>{this.props.title}</h1>
            <div>{products}</div>
            </div>
            );
    }
}