const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

//put in fct de id
app.put('/update/:id', (req,res)=>{
   const id = req.params.id;
   let updated = false;
   products.forEach((product) =>{
      if(product.id == id){
          updated = true;
          product.productName=req.body.productName;
          product.price=req.body.price;
      } 
   });
   if(updated){
    res.status(200).send(`Product ${id} updated!`);    
   } else {
       res.status(404).send(`Could not find resource with id ${id}`);
   }
   
});

//delete in fct de nume(payload)
app.delete('/delete', (req,res)=>{
    const name=req.body.productName;
    let deleted=false;
    products.forEach((p)=>{
        if(p.productName==name){
            const id=p.id;
            if(id<products.length){
                deleted=true;
                products.splice(id,1);
            }
        }
    });
    if(deleted){
    res.status(200).send(`Product ${name} deleted!`);    
   } else {
       res.status(404).send(`Could not find resource with id ${name}`);
   }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});